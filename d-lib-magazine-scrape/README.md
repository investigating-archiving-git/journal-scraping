D-Lib Magazine Scrape

This python script scrapes article titles from the D-Lib Magazine Author Index and then looks for articles with "software", "preservation", or "web archive" in the titles. The script removes duplicate entries and writes the results to three seperate json files based on title.  

