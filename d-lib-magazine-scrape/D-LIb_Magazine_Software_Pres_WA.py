# (1) this python script scrapes the D-Lib magazine author index 
# (2) looks for articles with "software" and "preservation" in the titles
# (3) turns the resulting lists into sets to remove duplicates
# (4) writes those results to two json files
from bs4 import BeautifulSoup
import requests
import json


url = "http://www.dlib.org/author-index.html"

author_index = requests.get(url)
author_index_html = author_index.text 
soup = BeautifulSoup(author_index_html, "html.parser")

all_articles = soup.find_all("p", attrs = {"class": "archive"})
#print(all_articles)

software_results = []
preservation_results = []
web_archive_results = []

for article in all_articles:

	my_data = {
		"title": None,
		"url": None,
	}

	# print ("---------")
	# print(article.text)

	article_link = article.find("a")
	
	try: 
		abs_url = "http://www.dlib.org/" + article_link['href']
		article_title = article_link.text
	except:
		pass

	my_data['title'] = article_title
	my_data['url'] = abs_url

	for key in my_data:
		x = "Software" 
		y = "Preservation"
		z = "Web Archiv"
		if x in my_data['title']:
			software_results.append(my_data['title'] + " | " + my_data['url'])
		if y in my_data['title']:
			preservation_results.append(my_data['title'] + " | " + my_data['url'])
		if z in my_data['title']:
			web_archive_results.append(my_data['title'] + " | " + my_data['url'])

software_list = (set(software_results))
preservation_list = (set(preservation_results))
web_archive_list = (set(web_archive_results))

counter = 1 
for item in software_list:
	print(counter, ":", item, "\n")
	counter = counter + 1

print ("----------------------\n")

counter = 1 
for item in preservation_list:
	print(counter, ":", item, "\n")
	counter = counter + 1

print ("----------------------\n")

counter = 1 
for item in web_archive_list:
	print(counter, ":", item, "\n")
	counter = counter + 1

# # following code writes the lists out to json file -- first writes software, second preservation

software_list2 = []

for item in software_list:
	software_list2.append(item)

with open('D-Lib_Articles_Software.json', 'a') as outfile:
	json.dump(software_list2, outfile, indent=2)
	print("your file is ready")

preservation_list2 = []

for item in preservation_list:
	preservation_list2.append(item)

with open('D-Lib_Articles_Pres.json', 'a') as outfile:
	json.dump(preservation_list2, outfile, indent=2)
	print("your file is ready")

web_archive_list2 = []

for item in web_archive_list:
	web_archive_list2.append(item)

with open('D-Lib_Articles_Web_Archive.json', 'a') as outfile:
	json.dump(web_archive_list2, outfile, indent=2)
	print("your file is ready")




