### About
This holds all codebase repositories used to scrape selected open access journals for the Sloan-funded project, *[Investigating and Archiving the Scholarly Git Experience](https://investigating-archiving-git.gitlab.io/)*. Each individual codebase repository includes APIs and scripts specific to scraping metadata and full-text from each journal or journal repository. 

### Journals included: 
- [Journal of Research Software](https://openresearchsoftware.metajnl.com/)
- [The Journal of Open Source Software](https://joss.theoj.org/)
- [D-Lib Magazine: The Magazine of Digital Library Research](http://dlib.org/)
- [PLOS: Public Library of Science](https://www.plos.org/) (All PLOS publications)
- [Journal of Statistical Software](https://www.jstatsoft.org/index)
