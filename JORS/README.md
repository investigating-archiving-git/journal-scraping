This script scrapes article urls, abstracts, keywords, and full-text from all articles in The Journal of Open Research Software (JORS). Content is written to JSON in the form of key-value pairs.
