# This python script scrapes urls, PDF links, titles,
# DOIs, and publication dates from all articles in The Journal of Statistical Software
# This is script 1 of 2
# Use script 2 to download PDFs

# import modules
from bs4 import BeautifulSoup
import requests
import json

all_my_data = []

for pages in range (0,53):
	url = f"https://www.jstatsoft.org/search/titles?searchPage={pages*1}#results"

	#request url and turn into soup object
	results_page = requests.get(url)
	page_html = results_page.text
	soup = BeautifulSoup(page_html, "html.parser")

	# identify table and table rows
	table = soup.find_all('table')[3]
	records = table.find_all('tr')

	for record in records:

		fields = record.find_all("td")

		for entry in fields:

			my_data = {
				"article_url": None,
				"article_pdf": None,
				"article_title": None,
				"DOI": None,
				"published_date": None, 
			}

			# identify urls for each article
			try:
				item_link = entry.find('a', attrs={'class':'file'})
				href = item_link['href']
				my_data["article_url"] = href

				# open article urls and turn into soup object
				article_page = requests.get(href)
				article_html = article_page.text
				article_soup = BeautifulSoup(article_html, "html.parser")

				# get metadata and assign to my_data dictionary
				all_field_divs = article_soup.find("meta", attrs={'name':'citation_fulltext_html_url'})
				pdf_link = all_field_divs['content']
				my_data["article_pdf"] = pdf_link
				
				title = article_soup.find("meta", attrs={'name':'citation_title'})
				title_text = title['content']
				my_data["article_title"] = title_text

				doi = article_soup.find("meta", attrs={'name':'DC.Identifier.DOI'})
				doi_text = doi['content']
				doi_text = doi_text.strip()
				my_data["DOI"] = doi_text

				published_date = article_soup.find("meta", attrs={'name':'citation_date'})
				date_text = published_date['content']
				my_data["published_date"] = date_text

				# append my_data dictionary to empty list
				all_my_data.append(my_data)

				# print(my_data)
				# print('--------')

			except TypeError:
				pass

# print(all_my_data)
# write to JSON 
		
with open('jStatSoft.json', 'w') as file_object:
	json.dump(all_my_data, file_object, indent=2)
	print('Your file is now ready')
	
			




