This script scrapes the pdf urls for all published articles from the Journal of Statistical Software and outputs those urls to JSON. A second script to download those pdfs is currently being written, but the description of it is below.

Script 1:
file name: jStatSoft_1_Articles_Metadata.py
This script is designed to scrape metadata for each article published by The Journal of Statistical Software. This information is saved to a list, which is written to a JSON file (jStatSoft_Articles.json). The script can be run as a stand alone for the metadata and JSON but requires Script 2 for full functionality.

Script 2:
file name: jStatSoft_2_Articles_Download.py
This script is designed to retrieve the article .pdf url links in the JSON file  (jStatSoft_Articles.json) created by Script 1. The pdfs are written to the local file in which the script is contained. This script is not a stand alone and requires the output from Script 1 for full functionality.  
