#This python script scrapes The Journal of Open Source Software
#metadata for every published article is collected and written to a JSON file
#this is script 1 of 2

#import modules
from bs4 import BeautifulSoup
import requests
import json

#create empty list for data
all_my_data = []

#use f-string synatax for paginating through pages
#(0,59) refers to the 58 pages of articles needed
for pages in range (0,59):
	url = f"https://joss.theoj.org/papers/popular?page={pages*1}"

	#request urls using the requests module and turn it into a soup object
	results_page = requests.get(url)
	page_html = results_page.text
	soup = BeautifulSoup(page_html, "html.parser")
	# print(soup)

	#identify html section holding metadata fields
	articles = soup.find_all("entry")
	# print(articles)

	#write for loop that captures metadata fields for each article
	#create data dictionary for all fields needed 
	for article in articles: 

		my_data = {
		"Article_url": None,
		"Title": None,
		"PDF_link": None,
		"Date": None,
		"Citation": None,
		"DOI": None,

		}

		#identify absolute url for each article
		#assign it to data dictionary
		# print('-----------')
		article_links = article.find('link')
		absolute_url = article_links['href']
		my_data["Article_url"] = absolute_url
		# print(absolute_url)

		#turn each absolute url into a beautifulsoup object and parse
		item_request = requests.get(absolute_url)
		item_html = item_request.text
		item_soup = BeautifulSoup(item_html, "html.parser")
		# print(item_soup)

		#identify and isolate title
		#assign title to data dictionary
		title = item_soup.find("meta", attrs={'property': 'og:title'})
		title_only = title['content']
		my_data["Title"] = title_only
		# print(title_only)

		#identify and isolate pdf url
		#assign pdf url to data dictionary
		pdf = item_soup.find("meta", attrs={'name': 'citation_pdf_url'})
		pdf_only = pdf['content']
		my_data['PDF_link'] = pdf_only
		# print(pdf_only)

		#identify and isolate publication date
		#assign publication date to data dictionary
		date = item_soup.find("meta", attrs={'name': 'citation_publication_date'})
		date_only = date['content']
		my_data['Date'] = date_only
		# print(date_only)

		#identify and isolate citation
		#assign citation to data dictionary
		citation = item_soup.find("meta", attrs={'property': 'og:description'})
		citation_only = citation['content']
		my_data['Citation'] = citation_only
		# print(citation_only)
		
		#identify and isolate doi
		#assign doi to data dictionary
		doi = item_soup.find("meta", attrs={'name': 'citation_doi'})
		doi_only = doi['content']
		my_data["DOI"] = doi_only
		# print(doi_only)

		#append data dictionary into a list
		all_my_data.append(my_data)
		# print(all_my_data)

# write out json
with open('JOSS_Articles.json', 'w') as file_object:
	json.dump(all_my_data, file_object, indent=2)
	print("Your JSON file is Ready")


