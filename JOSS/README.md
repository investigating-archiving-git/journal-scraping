These two scripts, which should be used in tandem, download published article pdfs from The Journal of Open Source Software.

Script 1:
file name: JOSS_1_Articles_Metadata.py
This script is designed to scrape metadata for each article published by The Journal of Open Source Software. Metadata includes article url, pdf url, title, citation, and DOI. The metadata information is saved to a dictionary, which is written to a JSON file (JOSS_Articles.json). The script can be run as a stand alone for the metadata and JSON but requires Script 2 for full functionality.

Script 2:
file name: JOSS_2_Articles_Download.py
This script is designed to retrieve the article .pdf url links in the JSON file  (JOSS_Articles.json) created by Script 1. The pdfs are written to the local file in which the script is contained. This script is not a stand alone and requires the output from Script 1 for full functionality.  