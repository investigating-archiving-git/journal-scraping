#This python script downloads articles from The Journal of Open Source Software
#it uses the JSON file created using script 1 (JOSS_1_Articles_Metadata.py)
#this is script 2 of 2

#import modules
import requests
import json

#open and load JSON created by script 1
#identify pdf links in JSON
#slice link url to get DOI and use as file name
with open("JOSS_Articles.json") as f_object:
	text = json.load(f_object)
	for data_dict in text:
		link = data_dict["PDF_link"]
		name = 'DOI_' + link[-23:]
		# print(name)

		#use requests to get and download article pdfs
		make_request = requests.get(link, stream=True)
		with open (name, 'wb') as pdf:
			for chunk in make_request.iter_content(chunk_size=1024):
				if chunk:
					pdf.write(chunk)




