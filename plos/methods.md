# Methodology using allofplos

Here are the basic installation and downloading steps to access all PLOS articles. This is mostly a copy-paste from the [All of Plos (allofplos) README.rst](https://github.com/PLOS/allofplos/blob/master/README.rst). 

1.    Make a virtual environment
```
$ virtualenv allofplos
```

2.    Use pip to install allofplos
```
(allofplos)$ pip install allofplos
```

3.    Run the program by executing this command
```
(allofplos)$ python -m allofplos.update
```

Once it runs, the PLOS Corpus will download onto your local drive. Each article (metatdata and full-text) will be in separate XML files.
   
